package singleton;

/**
 * 懒汉式改进(内部类被调用实现延迟装载)
 * Created by donggang on 18/1/22 星期一.
 */
public class MySingleton2a {
	public static class Singleton {
		private static MySingleton2a singleton = new MySingleton2a();
	}

	public static MySingleton2a getInstance() {
		MySingleton2a s1 = Singleton.singleton;
		return s1;
	}
	public static void main(String[] args) {
		MySingleton2a.getInstance();
	}
}


