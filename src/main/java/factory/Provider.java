package factory;

/**
 * Created by donggang on 18/1/22 星期一.
 */
public interface Provider {

	Sender produce();
}
