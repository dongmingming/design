package factory;

/**
 * Created by donggang on 18/1/22 星期一.
 */
public class TestFactory {
	public static void main(String[] args) {
		Sender mailSender = new SendMailFactory().produce();
		mailSender.send();

		Sender smsSender = new SendSmsFactory().produce();
		smsSender.send();

	}
}
