package factory;

/**
 * Created by donggang on 18/1/22 星期一.
 */
public class SendSmsFactory implements Provider{

	public Sender produce() {
		return new SmsSender();
	}
}
